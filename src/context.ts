import { tradeTokenForUser } from './controllers/user';

export async function context(param: any) {
  const req: any = param.req;
  let authToken = null;
  let user = null;

  try {
    authToken = req.headers['authorization'];

    if (authToken) {
      user = await tradeTokenForUser(authToken);

      if (!user) throw new Error('you must be logged in');
    }
  } catch (e) {
    console.warn(`Unable to authenticate using auth token: ${authToken}`);
  }

  return { user };
}
