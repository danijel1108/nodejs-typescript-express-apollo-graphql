import { GraphQLResolveInfo } from 'graphql';
import { IResolvers } from 'graphql-tools';
import { Context, IUser, IRole } from './models';
import { loginUser, getUser, getUserList, createUser, updateUser, deleteUser } from './controllers/user';
import { getRole, getRoleList, createRole, updateRole, deleteRole } from './controllers/role';

const resolverMap: IResolvers = {
  Query: {
    helloWorld(_: void, args: void, ctx: Context, info: GraphQLResolveInfo): string {
      return `👋 Hello world! 👋`;
    },
    user: (parent: any, { _id }: { _id: string }, context) => getUser(_id, context),
    users: (parent: any, context) => getUserList(),
    role: (parent: any, { _id }: { _id: string }) => getRole(_id),
    roles: (parent: any) => getRoleList(),
  },
  Mutation: {
    loginUser: (parent: any, { user }: { user: IUser }) => loginUser(user),
    createUser: (parent: any, { user }: { user: IUser }) => createUser(user),
    updateUser: (parent: any, { user }: { user: IUser }, context) => updateUser(user, context),
    deleteUser: (parent: any, { _id }: { _id: string }) => deleteUser(_id),
    createRole: (parent: any, { role }: { role: IRole }) => createRole(role),
    updateRole: (parent: any, { role }: { role: IRole }) => updateRole(role),
    deleteRole: (parent: any, { _id }: { _id: string }) => deleteRole(_id),
  },
  User: {
    role: async (parent: IUser, params: any, ctx: any, info: any) => {
      const { roleId } = parent;
      if (!roleId) return null;
      return await getRole(roleId);
    },
  },
};

export default resolverMap;
