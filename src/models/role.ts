import mongoose, { Schema } from 'mongoose';

const roleSchema = new Schema({
  name: String,
});

const Role = mongoose.model('roles', roleSchema);

export { Role };
