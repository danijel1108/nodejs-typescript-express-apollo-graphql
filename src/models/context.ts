interface Context {
  jwt?: String;
}

interface IUser {
  _id: string;
  name: string;
  password: string;
  roleId: string;
}

interface IRole {
  _id: string;
  name: string;
}


export { IUser, IRole, Context };
