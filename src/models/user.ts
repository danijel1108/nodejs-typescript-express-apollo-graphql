import mongoose, { Schema, mongo } from 'mongoose';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

const userSchema = new Schema({
  name: { type: String, required: true },
  password: { type: String, required: true },
  roleId: String,
});

userSchema.pre('save', function (next) {
  var user: any = this;

  // only hash the password if it has been modified (or is new)
  if (!user.isModified('password')) return next();

  // generate a salt
  bcrypt.genSalt(10, function (err, salt) {
    if (err) return next(err);

    // hash the password using our new salt
    bcrypt.hash(user.password, salt, function (err, hash) {
      if (err) return next(err);

      // override the cleartext password with the hashed one
      user.password = hash;
      next();
    });
  });
});

userSchema.methods.comparePassword = async function (candidatePassword: String) {
  return await bcrypt.compare(candidatePassword, this.password);
};

userSchema.methods.toJSON = function () {
  var obj = this.toObject();
  delete obj.password;
  return obj;
};

userSchema.methods.getJWT = function () {
  return jwt.sign({ _id: this._id }, 'rRw#LJY=2dz4@pF3', { expiresIn: '24h' });
};

const User = mongoose.model('users', userSchema);

export { User };
