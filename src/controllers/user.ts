import { User, IUser, Role } from '../models';
import { isEmpty } from 'lodash';
import jwt from 'jsonwebtoken';

export async function userLogout() {
}

export async function loginUser(user: IUser) {
  const dbUser: any = await User.findOne({ name: user.name, roleId: user.roleId });

  if (!dbUser) throw "User doesn't exist!"

  const compareResult = await dbUser.comparePassword(user.password);
  if (!compareResult) throw "Password is incorrect!";

  return { user: dbUser.toJSON(), token: dbUser.getJWT() };
}

export async function getUser(_id: string, context: any) {
  console.log("context", context)
  return await User.findOne({ _id });
}

export async function getUserList() {
  return await User.find();
}

export async function createUser(user: IUser) {
  const exist = await roleId_exist(user.roleId);
  return isEmpty(exist) ? null : await User.create(user);
}

export async function updateUser(user: IUser, context: any) {
  const exist = await roleId_exist(user.roleId);
  return isEmpty(exist) ? null : await User.findOneAndUpdate({ _id: user._id }, user, { new: true });
}

export async function deleteUser(_id: string) {
  const result = await User.deleteOne({ _id });
  return Boolean(String(result.ok));
}

export async function tradeTokenForUser(token: string) {
  const decode: any = jwt.decode(token);
  const user: any = await User.findById(decode._id);
  return user.toJSON();
}

async function roleId_exist(_id: string) {
  return await Role.findOne({ _id });
}
