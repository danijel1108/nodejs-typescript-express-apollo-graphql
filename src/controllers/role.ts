import { Role, IRole } from '../models';

export async function getRole(_id: string) {
  return await Role.findOne({ _id });
}

export async function getRoleList() {
  return await Role.find();
}

export async function createRole(role: IRole) {
  return await Role.create(role);
}

export async function updateRole(role: IRole) {
  return await Role.findOneAndUpdate({ _id: role._id }, role, { new: true });
}

export async function deleteRole(_id: string) {
  const result = await Role.deleteOne({ _id });
  return Boolean(String(result.ok));
}
