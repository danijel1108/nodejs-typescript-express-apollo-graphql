# nodejs-typescript-express-apollo-graphql-starter

[See tutorial on Medium](https://medium.com/@th.guibert/basic-apollo-express-graphql-api-with-typescript-2ee021dea2c)

## Package install
  - npm install

## How to execute this project
  - npm run start:dev

## How to build and execute
  - npm run build
  - npm run start

----

That is time running your project now. Please click [here](http://localhost:3000/graphql).

You can execute the queries you want

-----

## The list of possible queries
Here is queries for role collection
### CRUD for Role collection.
  * create a role
    ```
    Query:
      mutation($role: RoleInput!){
        createRole(role: $role){
          _id
          name
        }
      }

    Variable:
      {
        "role": {
          "name": "<here is role name>"
        }
      }
    ```
  * update a role
    ```
    Query:
      mutation($role: RoleUpdateInput!){
        updateRole(role: $role){
          _id
          name
        }
      }

    Variable:
      {
        "role": {
          "_id": "<here is new role id>",
          "name": "<here is new role name>"
        }
      }
    ```
  * delete a role
    ```
    Query:
      mutation ($_id: String!) {
        deleteRole(_id: $_id)
      }

    Variable:
      {
        "_id":"<here is role id>"
      }
    ```
  * read role
    - get all roles
      ```
      query{
        roles{
          _id
          name
        }
      }
      ```
    - get a role
      ```
      Query:
        query($_id: String!){
          role(_id: $_id){
            _id
            name
          }
        }

      Variable:
        {
          "_id":"<here is role id>"
        }
      ```
And then the time is for user collection
### CRUD for User collection.
  * create a user
    ```
    Query:
      mutation($user:UserInput!){
        createUser(user:$user){
          _id
          name
          roleId
        }
      }

    Variable:
      {
        "user": {
          "name": "<here is user name>",
          "roleId": "<here is role id>"
        }
      }
    ```
  * update a user
    ```
    Query:
      mutation($user:UserUpdateInput!){
        updateUser(user: $user){
          _id
          name
          roleId
        }
      }
    Variable:
      {
        "user": {
          "_id": "<here is user id>",
          "name": "<here is new username>",
          "roleId": "<here is new role id>"
        }
      }
    ```
  * delete a user
    ```
    Query:
      mutation($_id:String!){
        deleteUser(_id: $_id)
      }

    Variable:
      {
        "_id":"<here is user id>"
      }
    ```
  * read user
    - get all roles
      ```
      query{
        users{
          _id
          name
        }
      }
      ```
    - get a role
      ```
      Query:
        query($_id: String!){
          user(_id: $_id){
            _id
            name
          }
        }

      Variable:
        {
          "_id":"<here is user id>"
        }
      ```
